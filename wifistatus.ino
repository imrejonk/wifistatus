#include <ESP8266HTTPClient.h>
#include <ESP8266WiFi.h>
#include "config/esp8266.h"

HTTPClient http;
String     httpBody;
int        httpStatus;
byte       mac[6];

void setup() {
  // Initialize serial communication at 115200 baud
  Serial.begin(115200);

  // Print MAC address
  WiFi.macAddress(mac);
  Serial.print("\nMy MAC address is: ");
  Serial.print(mac[0],HEX);
  Serial.print(":");
  Serial.print(mac[1],HEX);
  Serial.print(":");
  Serial.print(mac[2],HEX);
  Serial.print(":");
  Serial.print(mac[3],HEX);
  Serial.print(":");
  Serial.print(mac[4],HEX);
  Serial.print(":");
  Serial.println(mac[5],HEX);

  // Connect to the WiFi network
  Serial.print("Connecting to SSID ");
  Serial.print(NETWORK);
  WiFi.begin(NETWORK, PASSWORD);

  // Loop until connected
  while (WiFi.status() != WL_CONNECTED) {
    delay(1000);
    Serial.print(".");
  }

  Serial.print("\nConnected! IP address: ");
  Serial.println(WiFi.localIP());
  Serial.println("");
}

void loop() {
  // Construct HTTP body containing the signal strength
  httpBody = "rssi=" + String(WiFi.RSSI());

  // Print HTTP body to serial console for debugging
  Serial.println(httpBody);

  // Send RSSI value to the web server
  http.begin(URL);
  http.addHeader("Content-Type", "text/plain");
  httpStatus = http.PUT(httpBody);

  if (httpStatus > 0) {
    // Web server responded, print status code for debugging
    Serial.printf("HTTP server response: %i\n", httpStatus);
  }
  else {
    // Sending request failed, print client error message
    Serial.printf("HTTP client error: %s\n", http.errorToString(httpStatus).c_str());
  }

  // Sleep one second, then run the loop again
  delay(1000);
}
