# WiFi Status

Code for a simple ESP8266 RSSI sensor, API and web app to monitor WiFi signal strength. I wrote this for a study assignment.  
Author: Imre Jonk <mail@imrejonk.nl>  
Home page: https://www.imrejonk.nl/wifistatus/

## Used techologies

Languages:
- Python 3
- C++
- HTML 5
- JavaScript
- CSS 3

Frameworks:
- Django 2

Libraries:
- Bootstrap 4
- jQuery 3
- ESP8266HTTPClient
- ESP8266WiFi

DevOps cycle:
- GitLab CI/CD for continuous deployments
- Docker in production 😱

## Quick development setup

1. Install the Node.js dependencies in package.json and the Python dependencies in requirements.txt
2. Create the SQLite database with `./manage.py migrate`
3. Run the backend with `./manage.py runserver`
4. Copy config/esp8266.example.h to esp8266.h and make the necessary changes
5. Use the Arduino IDE to compile the C++ code in wifistatus.ino and upload it to your ESP8266 board
6. Use the serial monitor (115200 baud) to debug any connection problems

## Overview diagram

```
-----------     ------     -----------     --------------
| ESP8266 | <-> | AP | <-> | Network | <-> | Django     |
|  board  |     ------     -----------     | web server |
-----------  ∧                  ∧          --------------
             |                  |
      ESP8266 measures          ∨
      RSSI on this link     ----------
                            | Client |
                            ----------
```

## Sensor

This project was developed for the WEMOS D1 mini ESP8266 board. It has a built-in WiFi chip which is controlled using [ESP8266WiFi](https://github.com/esp8266/Arduino/tree/master/libraries/ESP8266WiFi). The C++ code in wifistatus.ino takes care of connecting to the access point defined in config/esp8266.h. It also takes care of sending HTTP PUT requests containing the WiFi signal strength (RSSI value) with [ESP8266HTTPClient](https://github.com/esp8266/Arduino/tree/master/libraries/ESP8266HTTPClient) to a URL defined in the same configuration file.

## Web app and API

I picked Django as the web framework for this project because I already have some experience with it from prior projects and because I wanted to dive more deeply in those damn delicious Django docs. The HTML template uses Bootstrap, simply because the assignment required it.

Django can be seen as a model-view-controller (MVC) framework. This project requires only one model which I called 'Status'. It contains a timestamp and the RSSI value, both of which can be updated by sending an HTTP PUT request to the controller which updates the view (by default at /rssi). The view only shows the RSSI value, or 'unknown' if the value has not been updated recently. This value is loaded into the front end through AJAX calls which are executed by jQuery. Bootstrap takes care of the alert box and makes the interface a bit more responsive.

## Production deployment

The .gitlab-ci file contains the script that is used to set up the Docker containers in production. The first container, wifistatus-builder, is a NodeJS container which runs a script that uses [Yarn](https://yarnpkg.com/) to download the front end dependencies. The second container runs the apache2 web server with mod\_wsgi to provide the Django back-end. I personally use MariaDB Server but you can [adapt](https://docs.djangoproject.com/en/stable/ref/settings/#databases) the database configuration in settings.py to suit your needs.

## Security

The API calls from the sensor are transmitted over plain HTTP. ESP8266HTTPClient does support HTTPS but requires hard-coding the server's certificate fingerprint, which I hardly find a solution at all. There is also no API authentication built-in, which means that anyone can update the RSSI value.
