from django.db import models


class Status(models.Model):
    # Timestamp field, default value is the current timestamp
    time = models.DateTimeField(auto_now_add=True)

    # RSSI field
    rssi = models.IntegerField()
