import pytz

from core.models import Status
from datetime import timedelta
from django.http import HttpResponse
from django.http import QueryDict
from django.shortcuts import render
from django.template import loader
from django.utils import timezone


def index(request):
    # This is the index page

    # Return HTML status page
    template = loader.get_template('index.html')
    return HttpResponse(template.render())


def rssi(request):
    # This is the /rssi page, used for retrieving and updating the RSSI value

    # Create QueryDict object containing the request body
    qd = QueryDict(request.body)

    if request.method == 'PUT' and 'rssi' in qd:
        # This is an update (PUT) request

        # Print RSSI value for debugging
        print('Received new RSSI value: ' + qd.get('rssi'))

        # Save new RSSI value to the database
        status = Status(rssi=qd.get('rssi'))
        status.save()

        # Return an empty HTTP response
        return HttpResponse()
    else:
        # This is a GET request

        try:
            # Retrieve the latest RSSI value that is no older than five seconds
            status = Status.objects.filter(
                time__gte=timezone.now() - timedelta(seconds=5)
            ).latest('id')
            rssi = status.rssi
        except Status.DoesNotExist:
            # No recent RSSI value available
            rssi = 'unknown'

        # Return only the RSSI value, or 'unknown'
        return HttpResponse(rssi)
