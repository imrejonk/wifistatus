FROM python:3.7

WORKDIR /app
COPY . .

RUN apt update; \
    apt install -y apache2 apache2-dev; \
    pip install -r requirements.txt; \
    pip install -r docker/requirements.txt; \
    cp docker/apache2.conf /etc/apache2/sites-enabled/000-default.conf; \
    rm -r /var/lib/apt/lists/*

EXPOSE 80

ENTRYPOINT ["/app/docker/wifistatus.sh"]
