#!/bin/sh

# Apply any unapplied database migrations
/app/manage.py migrate --noinput

# Collect static files into STATIC_ROOT
/app/manage.py collectstatic --noinput

# Run apache2 web server
/usr/sbin/apache2ctl -D FOREGROUND
