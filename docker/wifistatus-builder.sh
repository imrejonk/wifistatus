#!/bin/sh

cd /app

# Install Node.JS packages with Yarn (https://yarnpkg.com/)
yarn

# Fix file ownership
chown -R --reference manage.py *
